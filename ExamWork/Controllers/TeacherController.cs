﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExamWork.Models;
using ExamWork.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ExamWork.Controllers
{
    public class TeacherController : Controller
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public TeacherController(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }
        public IActionResult Index()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var teachers = unitOfWork.Teachers.GetAll();
                return View(teachers);
            }

        }
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create( Teacher teacher)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                unitOfWork.Teachers.Add(teacher);
                return RedirectToAction("Index");
            }
        }

        public IActionResult Delete(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                Teacher tchId = unitOfWork.Teachers.GetById(id);
                unitOfWork.Teachers.Delete(tchId);
            }
            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                Teacher teacherIdObject = unitOfWork.Teachers.GetById(id);
                return View(teacherIdObject);
            }

        }
        [HttpPost]
        public IActionResult Edit(Teacher teacher)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                unitOfWork.Teachers.Update(teacher);
            }
            return RedirectToAction("Index");

        }

    }
}