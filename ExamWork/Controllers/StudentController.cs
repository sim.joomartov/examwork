﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExamWork.Models;
using ExamWork.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ExamWork.Controllers
{
    public class StudentController : Controller
    {
        private IUnitOfWorkFactory _unitOfWorkFactory;

        public StudentController(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }
        public IActionResult Index()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var studets = unitOfWork.Students.GetAll();
                return View(studets);
            }
            
        }

        public IActionResult Create()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                ViewBag.Teachers = new SelectList(unitOfWork.Teachers.GetAll(), "Id", "FirstName");
                return View();
            }
        }

        [HttpPost]
        public IActionResult Create(Student student)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                unitOfWork.Students.Add(student);
            }
            

            return RedirectToAction("Index");

        }

        public IActionResult Delete(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                Student StdId = unitOfWork.Students.GetById(id);
                unitOfWork.Students.Delete(StdId);
            }
            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                Student studentIdobject = unitOfWork.Students.GetById(id);
                ViewBag.Teachers = new SelectList(unitOfWork.Teachers.GetAll(), "Id", "FirstName");
                return View(studentIdobject);
            }
        }

        [HttpPost]
        public IActionResult Edit(Student student)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                unitOfWork.Students.Update(student);
            }
            return RedirectToAction("Index");
        }
    }
}