﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExamWork.Models
{
    public class Teacher : Entity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string TeacherSubject { get; set; }

        public ICollection<Student> Students { get; set; }

        public Teacher()
        {
            Students = new List<Student>();
        }
    }

}
