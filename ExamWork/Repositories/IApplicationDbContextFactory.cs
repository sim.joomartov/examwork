﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExamWork.Repositories
{
    public interface IApplicationDbContextFactory
    {
        ApplicationDbContext Create();
    }
}
