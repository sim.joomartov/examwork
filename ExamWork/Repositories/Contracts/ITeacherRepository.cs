﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExamWork.Models;

namespace ExamWork.Repositories.Contracts
{
    public interface ITeacherRepository : IRepository<Teacher>
    {
    }
}
